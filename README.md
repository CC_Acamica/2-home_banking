**Segundo proyecto de la carrera Desarrollo Web Full Stack de Acámica. El objetivo es darle funcionalidad mediante Javascript a la aplicación. El html y CSS inicial fue brindado por el instituto.**

---

## Objetivo del proyecto
En este proyecto vas a crear tu primer programa utilizando JavaScript, el principal lenguaje de programación web. El proyecto consiste en simular un Home Banking, donde el usuario de la aplicación puede ver el saldo de su cuenta y simular acciones como extracción, depósito y transferencias de dinero o pago de servicios.

Realizando este programa vas a poner en práctica los fundamentos de la programación, las bases comunes a todos los lenguajes de programación y a todos los sitios web que existen.

---

## Proyecto finalizado
El proyecto finalizado se encuentra en el directorio ChristianCabrer-HomeBanking.
Se puede ejecutar el mismo abriendo el archivo index.html en el navegador y poniendo el código de seguridad solicitado: 9836

---

## Descripción de como se llevo a cabo el proyecto
* Primero comencé viendo el funcionamiento de los archivos que ya estaban funcionando: HTML Y CSS.
* Luego empece a identificar las nuevas funcionalidades que se le debian dar al proyecto.
* Login(con un único usuario y código fijo), extraer y depositar dinero, pagar servicios(con montos fijos), transferir dinero a otras cuentas ya cargadas y cambiar el límite de extracción.
* Para que el proyecto funcione correctamente se debia modificar el DOM a medida que se realizan ciertas operaciones para que el usuario vea los cambios y se agregaron validaciones para controlar que los datos ingresados por el usuario sean correctos. Y también el bloqueo de cuenta al ingresar mal la clave mas de 3 veces.

---