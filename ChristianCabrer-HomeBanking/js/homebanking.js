//Declaración de variables globales
var nombreUsuario = "Christian Cabrer";
var codigoSeguridad = "9836";
var saldoCuenta = 10000;
var limiteExtraccion = 2000;
var cuentaBloqueda = true;

//Ejecución de las funciones que inicializan la aplicacón.
window.onload = function () {
    iniciarSesion();
    cargarNombreEnPantalla();
    actualizarSaldoEnPantalla();
    actualizarLimiteEnPantalla();
};

//Funcion que modifica el limite de extracción.
function cambiarLimiteDeExtraccion() {
    //Se valida si la cuenta está bloqueada.
    if (ValidarCuentaBloqueada() !== false) {
        //Se le solicita al usuario que ingrese el nuevo limite deseado.
        var NuevoLimiteExtraccion = prompt("Ingrese el nuevo limite de extracción");
        //Se valida si el usuario toco el botón de cancelar.
        if (NuevoLimiteExtraccion !== null) {
            //Se valida si el usuario ingresó números.
            if (ValidarMontoValido(NuevoLimiteExtraccion) === false) {
                //Si no ingreso números se muestra un mensaje y se vuelve a llamar a la función para
                //que comience el proceso nuevamente.
                alert("Debe ingresar un monto válido.");
                cambiarLimiteDeExtraccion();
            }
            else {
                //Se convierte el valor ingresado a un int y se actualiza el valor en pantalla.
                limiteExtraccion = parseInt(NuevoLimiteExtraccion);
                actualizarLimiteEnPantalla();
                alert("Su nuevo limite de extraccion es de: $" + limiteExtraccion);
            }
        }
    }
}

//Funcion para extraer dinero de la cuenta.
function extraerDinero() {
    //Se valida si la cuenta está bloqueada.
    if (ValidarCuentaBloqueada() !== false) {
        //Se le solicita al usuario que ingrese el monto a extraer.    
        var dineroAextraer = prompt("Ingrese la cantidad de dinero que desea extraer");
        //Se valida si el usuario toco el botón de cancelar.
        if (dineroAextraer !== null) {
            //Se valida si el usuario ingresó números.
            if (ValidarMontoValido(dineroAextraer) === false) {
                //Si no ingresó números se muestra un mensaje y se vuelve a llamar a la función para
                //que comience el proceso nuevamente.
                alert("Debe ingresar un monto válido.");
                extraerDinero();
            }
            else {
                //Se convierten los valores a int.
                dineroAextraer = parseInt(dineroAextraer);
                limiteExtraccion = parseInt(limiteExtraccion);
                saldoCuenta = parseInt(saldoCuenta);
                //Se valida si el monto ingresado es múltiplo de 100.
                if (ValidarBilletes(dineroAextraer, 100) === false) {
                    //Si no ingresó un monto múltiplo de 100 se muestra un mensaje y se vuelve a llamar
                    //a la función para que comience el proceso nuevamente.
                    alert("Este cajero solo puede entregar billetes de $100.");
                    extraerDinero();
                }
                //Se valida si el monto ingresado supera el limite de extracción.
                else if (ValidarLimiteExtraccion(dineroAextraer) === false) {
                    //Si se ingresó un monto mayor al límite se muestra un mensaje y se vuelve a llamar
                    //a la función para que comience el proceso nuevamente.
                    alert("Esta tratando de extraer mas que su limite permitido.");
                    extraerDinero();
                }
                //Se valida si el monto ingresado supera el saldo disponible.
                else if (ValidarSaldoDisponible(dineroAextraer) === false) {
                    //Si se ingresó un monto mayor al saldo disponible se muestra un mensaje y se vuelve
                    //a llamar a la función para que comience el proceso nuevamente.
                    alert("No tiene saldo suficiente para la operación.");
                    extraerDinero();
                }
                else {
                    //Si todas las validaciones son exitosas se resta el dinero, se muestra el mensaje
                    //satisfactorio y se actualiza el saldo en pantalla.
                    var SaldoAntesDeDeposito = saldoCuenta;
                    RestarDinero(dineroAextraer);
                    MensajeDeSaldoActualizado(SaldoAntesDeDeposito, dineroAextraer, "E");
                    actualizarSaldoEnPantalla();
                }
            }
        }
    }
}

//Funcion para depositar dinero de la cuenta.
function depositarDinero() {
    //Se valida si la cuenta está bloqueada.
    if (ValidarCuentaBloqueada() !== false) {
        //Se le solicita al usuario que ingrese el monto a depositar.  
        var dineroAdepositar = prompt("Ingrese la cantidad de dinero que desea depositar");
        //Se valida si el usuario toco el botón de cancelar.
        if (dineroAdepositar !== null) {
            //Se valida si el usuario ingresó números.
            if (ValidarMontoValido(dineroAdepositar) === false) {
                //Si no ingresó números se muestra un mensaje y se vuelve a llamar a la función para
                //que comience el proceso nuevamente.
                alert("Debe ingresar un monto válido.");
                depositarDinero();
            }
            else {
                //Si todas las validaciones son exitosas se suma el dinero, se muestra el mensaje
                //satisfactorio y se actualiza el saldo en pantalla.
                var SaldoAntesDeDeposito = saldoCuenta;
                SumarDinero(dineroAdepositar);
                MensajeDeSaldoActualizado(SaldoAntesDeDeposito, dineroAdepositar, "D");
                actualizarSaldoEnPantalla();
            }
        }
    }
}

//Funcion para pagar servicios.
function pagarServicio() {
    //Se valida si la cuenta está bloqueada.
    if (ValidarCuentaBloqueada() !== false) {
        //Se le solicita al usuario que ingrese el servicio que desea pagar.  
        var SeleccionUsuario = prompt("1 - Agua\n" + "2 - Luz\n" + "3 - Internet\n" + "4 - Telefono");
        //Se valida si el usuario toco el botón de cancelar.
        if (SeleccionUsuario !== null) {
            //Se valida si el servicio seleccionado esta dentro de las opciones y se obtiene el precio.
            var PrecioServicio = ObtenerPrecioServicio(SeleccionUsuario);
            PrecioServicio = parseInt(PrecioServicio);
            if (PrecioServicio == 0) {
                //Si el servicio ingresado no existe se muestra el mensaje y se vuelve a llamar a la
                //función para que comience el proceso nuevamente.
                alert("El servicio ingresado no es valido.");
                pagarServicio();
            }
            else {
                //Se valida si la cuenta tiene saldo suficiente para pagar el servicio.
                if (ValidarSaldoDisponible(PrecioServicio) === false) {
                    alert("No tiene saldo suficiente para la operación.");
                }
                else {
                    //Si todas las validaciones son exitosas se resta el dinero, se muestra el mensaje
                    //satisfactorio y se actualiza el saldo en pantalla.
                    var SaldoAntesDeDeposito = saldoCuenta;
                    RestarDinero(PrecioServicio);
                    MensajeDeSaldoActualizado(SaldoAntesDeDeposito, PrecioServicio, SeleccionUsuario);
                    actualizarSaldoEnPantalla();
                }
            }
        }
    }
}

function transferirDinero() {
    //Se valida si la cuenta está bloqueada.
    if (ValidarCuentaBloqueada() !== false) {
        //Se le solicita al usuario que ingrese el monto a transferir.  
        var dineroATransferir = prompt("Ingrese la cantidad de dinero que desea transferir");
        //Se valida si el usuario toco el botón de cancelar.
        if (dineroATransferir !== null) {
            //Se valida si el usuario ingresó números.
            if (ValidarMontoValido(dineroATransferir) === false) {
                //Si no ingresó números se muestra un mensaje y se vuelve a llamar a la función para
                //que comience el proceso nuevamente.
                alert("Debe ingresar un número válido.");
                transferirDinero();
            }
            else {
                dineroATransferir = parseInt(dineroATransferir);
                saldoCuenta = parseInt(saldoCuenta);
                //Se valida si tiene saldo disponible para realizar la transferencia.
                if (ValidarSaldoDisponible(dineroATransferir) === false) {
                    //Si el saldo no es suficiente se muestra un mensaje y se vuelve a llamar 
                    //a la función para que comience el proceso nuevamente.
                    alert("No tiene saldo suficiente para la operacion.");
                    transferirDinero();
                }
                else {
                    //Se solicita al usuario que ingrese la cuenta a transferir.
                    var CuentaATransferir = prompt("Ingrese la cuenta a la que desea transferir");
                    //Se valida si el usuario toco el botón de cancelar.
                    if (CuentaATransferir !== null) {
                        //Se valida si la cuenta ingresada esta registrada.
                        if (ValidarCuentas(CuentaATransferir) === false) {
                            alert("La cuenta ingresada no está registrada.");
                        }
                        else {
                            //Si todas las validaciones son exitosas se resta el dinero, se muestra el mensaje
                            //satisfactorio y se actualiza el saldo en pantalla.
                            var SaldoAntesDeDeposito = saldoCuenta;
                            RestarDinero(dineroATransferir);
                            alert("Se transfirieron: $" + dineroATransferir + "\n A la cuenta: " + CuentaATransferir);
                            actualizarSaldoEnPantalla();
                        }
                    }
                }
            }
        }
    }
}

function iniciarSesion() {
    var CodigoCorrecto = false;
    var ContadorDeErrores = 0;
    //Se utiliza un ciclo para validar si el usuario ingreso la contraseña de manera erronea 3 veces
    //para proceder con el bloqueo de la misma.
    do {
        //Se le solicita al usuario que ingrese el código de seguridad.
        var Codigo = prompt("Ingrese el código de seguridad.");
        //Se valida si el código es correcto.
        if (codigoSeguridad != Codigo) {
            //Si el código ingresado es incorrecto se aumenta el contador en 1 y se muestra el mensaje.
            ContadorDeErrores++;
            alert("El código ingresado es incorrecto. Intento " + ContadorDeErrores + " de 3");
            if (ContadorDeErrores == 3) {
                //Si se llega a 3 errores se sale del ciclo de manera forzada.
                break;
            }
        }
        else {
            //Si el código ingresado es correcto se cambia a true el valor de la variable para salir
            //del ciclo.
            CodigoCorrecto = true;
        }
    } while (CodigoCorrecto === false);
    //Si el codigo fue ingresado mas de 3 veces de manera erronea, se muestra el mensaje y se bloquea la cuenta
    if (CodigoCorrecto === false) {
        alert("El código ingresado es incorrecto. Por seguridad su cuenta ha sido bloqueada.");
        saldoCuenta = 0;
        nombreUsuario = "";
        limiteExtraccion = 0;
        cuentaBloqueda = true;
    }
    else {
        //Si se ingresó el código correcto se loguea el usuario.
        alert("Bienvenido/a " + nombreUsuario);
        cuentaBloqueda = false;
    }
}

//Funciones que actualizan el valor de las variables en el HTML
function cargarNombreEnPantalla() {
    document.getElementById("nombre").innerHTML = "Bienvenido/a " + nombreUsuario;
}

function actualizarSaldoEnPantalla() {
    document.getElementById("saldo-cuenta").innerHTML = "$" + saldoCuenta;
}

function actualizarLimiteEnPantalla() {
    document.getElementById("limite-extraccion").innerHTML = "Tu límite de extracción es: $" + limiteExtraccion;
}

//Funciones que actualizan las variables
function SumarDinero(dinero) {
    dinero = parseInt(dinero);
    saldoCuenta += dinero;
}

function RestarDinero(dinero) {
    dinero = parseInt(dinero);
    saldoCuenta -= dinero;
}

//Funcion para mostrar mensajes
function MensajeDeSaldoActualizado(SaldoAnterior, Dinero, TipoActualizacion) {
    switch (TipoActualizacion) {
        case "D":
            alert("Has depositado: $" + Dinero + "\n Saldo Anterior: $" + SaldoAnterior + "\n Saldo actual: $" + saldoCuenta);
            break;
        case "E":
            alert("Has extraido: $" + Dinero + "\n Saldo Anterior: $" + SaldoAnterior + "\n Saldo actual: $" + saldoCuenta);
            break;
        case "1":
            alert("Has pagado el servicio de agua.\n Dinero descontado: $" + Dinero + "\n Saldo Anterior: $" + SaldoAnterior + "\n Saldo actual: $" + saldoCuenta);
            break;
        case "2":
            alert("Has pagado el servicio de luz.\n Dinero descontado: $" + Dinero + "\n Saldo Anterior: $" + SaldoAnterior + "\n Saldo actual: $" + saldoCuenta);
            break;
        case "3":
            alert("Has pagado el servicio de internet.\n Dinero descontado: $" + Dinero + "\n Saldo Anterior: $" + SaldoAnterior + "\n Saldo actual: $" + saldoCuenta);
            break;
            case "4":
            alert("Has pagado el servicio de telefono.\n Dinero descontado: $" + Dinero + "\n Saldo Anterior: $" + SaldoAnterior + "\n Saldo actual: $" + saldoCuenta);
            break;
    }
}

//Funciones que realizan validaciones
function ValidarCuentaBloqueada() {
    if (cuentaBloqueda === true) {
        alert("La cuenta está bloqueada y no puede ser utilizada.");
        return false;
    }
    else {
        return true;
    }
}

function ValidarMontoValido(MontoIngresado) {
    respuesta = true;
    if (isNaN(MontoIngresado) || MontoIngresado == "") {
        respuesta = false;
    }
    else if (MontoIngresado < 0) {
        respuesta = false;
    }
    return respuesta;
}

function ValidarSaldoDisponible(Monto) {
    if (Monto > saldoCuenta) {
        return false;
    }
    else {
        return true;
    }
}

function ValidarLimiteExtraccion(Monto) {
    if (Monto > limiteExtraccion) {
        return false;
    }
    else {
        return true;
    }
}

function ValidarBilletes(Monto, Billete) {
    var EsMultiplo = Monto % Billete;
    if (EsMultiplo != 0) {
        return false;
    }
    else {
        return true;
    }
}

//Función para obtener los precios de los distintos servicios.
function ObtenerPrecioServicio(Servicio) {
    switch (Servicio) {
        case "1":
            //Agua
            return 350;
            break;
        case "2":
            //Luz
            return 210;
            break;
        case "3":
            //Internet
            return 570;
            break;
        case "4":
            //Telefono
            return 425;
            break;
        default:
            //Ningun servicio valido
            return 0;
            break;
    }
}

//Función para validar si la cuenta ingresada es correcta.
function ValidarCuentas(Cuenta) {
    switch (Cuenta) {
        case "1234567":
        case "7654321":
            return true;
        default:
            return false;
            break;
    }
}